import discord
import random
import re
import os

from dotenv import dotenv_values

# Get environment variables
config = dotenv_values('.env')

aaronisms = ["Sheeeeeeesssh", "Chupapi muñañyo", "Chupapi", "Bityotaso", "Yoouuuuu knoooowwww meeee", "Wait wait wait wait wait", "Huuuuhhhhhhh", "Berrrraaaaaaaaa"]
client = discord.Client()

@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    text = message.content.lower()
    if message.content.startswith("!aaronsing"):
        try:
            channel = message.author.voice.channel
            vc = await channel.connect()
            # Get all aaronisms mp3s and play them at random
            base_path = "./aaronisms"
            mp3s = [os.path.join(base_path, mp3) for mp3 in os.listdir(base_path)]
            vc.play(discord.FFmpegPCMAudio(random.choice(mp3s)), after=lambda e: print("done", e))
            while vc.is_playing(): # This is the most disgusting thing I have ever done
                continue
            await vc.disconnect()
        except AttributeError:
            print("user is not in a vcoice channel and can not call aaron to sing")
    elif re.search("mommy", text):
        await message.channel.send('Sorry')
    elif re.search("aaron", text):
        await message.channel.send(random.choice(aaronisms))


client.run(config['DISCORD_SECRET'])