# Mommy Bot Documentation

### Setup

Prior to running mommy bot locally you will need a couple of dependencies.

#### Step 1

_You can skip this step if you already have python and pip on your machine_

First of all you are going to need python to run this project! You can download the [latest version here](https://www.python.org/downloads/)

Test that python was downloaded to your machine by going to a terminal and typing in `python --version`. Make sure the version listed matches the one you downloaded.

**Note Windows users may require additional setup and may need to download WSL.**

#### Step 2

Now you need to download all the python dependencies. To do so run the following command in the **root project directory**:

`pip3 install -r requirements.txt`

### Running the bot

To run the bot all you need to do is type the following command in the **projects root dir**:

`python3 mommy_bot.py`

### Adding to the bot

Feel free to add a PR to the repo and I will take a look at it!
